Bienvenido al repositorio principal de Miridia, este repositorio es público y de libre acceso. Favor de no apropiarse del software ni de sus componentes para fines comerciales o reventa, si decide usarlo para proyectos personales o empresariales tenga en cuenta el estado del desarrollo del software y las capacidades de este.

Sientase libre de usar, modificar y agregar nuevas funcionalidades al sistema del software, al igual que distribuirlo gratuitamente. Las únicas limitantes son:

- Apropiarse de la marca y/o producto, así como sus logos e imagenes originales. Se reserva el derecho de la marca, logos y personajes propios de la marca.
- Distribución del software original bajo licencia privativa.
- Uso abusivo o malicioso del software.

Dado lo anterior sientase libre de usar el software como "open source". Miridia, como software, queda licenciado bajo GPL v3

# Documentación
Para ver la documentación más reciente acerca del uso del software dirijase a la sección de "wiki" presente en éste repositorio.

# Registro de cambios
Se habilitará un registro de cambios al momento de pasar del periodo de desarrollo pre-alpha.