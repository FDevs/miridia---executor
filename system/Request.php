<?php
    //Modulo interno, encargado de extraer y dar formato a las entradas de las consultas
    namespace system;

    use src\Routes;

    class Request{
        public static function Getter($request){
            $response=[];
            if(is_array($request)){
                foreach($request as $method){
                    $response[$method]=self::{$method}();
                }
                return (Object)$response;
            }
            return NULL;
        }
        public static function URI(){
            $URI=$_SERVER['REQUEST_URI'];
            $router=new Routes();
            $router->routing();
            $compare=0;
            $res=NULL;
            if(isset($router->routes->routes->get)){
                foreach($router->routes->routes->get as $route){
                    [$compare_buffer,$vars]=$router->routes->compareURI($route,$URI);
                    if(($compare<$compare_buffer && $compare_buffer>0) || ($compare_buffer==-1 && $compare<1)){
                        $res=(Object)$vars;
                        $compare=$compare_buffer;
                    }
                }
            }
            return $res;
        }
        public static function GET(){
            $values=[];
            foreach($_GET as $get){
                $values[key($_GET)]=$get;
                next($_GET);
            }
            return (Object)$values;
        }
        public static function POST(){
            $values=[];
            foreach($_POST as $post){
                $values[key($_POST)]=$post;
                next($_POST);
            }
            return (Object)$values;
        }
    }