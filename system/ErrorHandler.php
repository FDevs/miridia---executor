<?php
    namespace system;

    use system\System;

    //Devuelve mensajes de errores,avisos o alertas personalizados con el fin de ser más amigables con el desarrollador
    class ErrorHandler{
        //Aisla el nombre del archivo
        public static function IdentifyFile($pathFile){
            $DIR=str_replace("\\","/",__DIR__);
            $file=str_replace("\\","/",$pathFile);
            $file=str_replace($DIR,"",$file);
            return $file;
        }
        public static function IdentifyFunction($file,$line){
            $content=System::read($file,'r');
            $content=explode("\n",$content);//se divide el archivo por lineas
            $function="";
            for($i=0;$i<=$line+1;$i++){//se recorre linea a linea del archivo
                $l=str_replace('  ','',$content[$i]);
                $elements=explode('public static function ',$l);
                if(count($elements)==2 && $i<=$line)//Si la linea es una funcion y es menor o igual a la linea de error
                    $function=explode("(",$elements[1])[0];//se extrae solo el nombre de la funcion
                $elements=explode('public function ',$l);
                if(count($elements)==2 && $i<=$line)
                        $function=explode("(",$elements[1])[0];//se extrae solo el nombre de la funcion
            }
            return $function;
        }
        //devuelve mensaje de alerta personalizado
        public static function Warning($file,$line){
            $filePathError=ErrorHandler::IdentifyFile($file);
            $configPath=System::dirBase().'/system/ErrorList'.str_replace(".php","",$filePathError).'.json';
            if(file_exists($configPath)){
                $data=json_decode(file_get_contents($configPath),true);//lista de errores desde json
                $function=self::IdentifyFunction($file,$line);
                if(key_exists($function,$data))
                    return $data[$function]["warning"]."</br>";
                else
                    return NULL;
            }else
                return NULL;
        }

        /*Manejador de errores de estado*/
        private static function getTitlePage($content){
            $title="";
            $bufferPage=explode("\n",$content);
            $i=0;
            foreach($bufferPage as $element){
                $buffer=str_replace(' ','',$element);
                if(substr($buffer,0,6)=="@title"){
                    $content=str_replace($bufferPage[$i],'',$content);
                    $title=str_replace('@title{','',$buffer);
                    $title=str_replace('}','',$title);
                    break;
                }else
                    $i++;
            }
            return [$content,$title];
        }
        //Devuelve error 404
        public static function Error404(){
            header('HTTP/1.1 404');
            $dir=str_replace("\\","/",__DIR__);
            $dir=str_replace("system","",$dir);
            $config=json_decode(file_get_contents($dir.'system/config/system.json'),true)["Error"];
            if(file_exists($dir.$config["dir"]."/".$config["list"]["404"])){
                $content=system::read($dir.$config["dir"]."/".$config["list"]["404"]);
                print $content;
            }else{
                print "Error 404: Contenido no encontrado";
            }
        }
    }
