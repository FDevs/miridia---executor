<?php
    namespace system;

    //Clase que contiene las funcionalidades para almacenar una ruta con la configuración dada (controlador)
    class Route{
        public $route=null;
        public $class=null;
        public $method=null;
        public function __construct($route,$callback){
            $this->route=$route;
            if($callback!=null){
                [$class,$method]=explode('::',$callback);
                $this->class=$class;
                $this->method=$method;
            }
        }

    }