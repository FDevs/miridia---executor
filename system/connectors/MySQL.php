<?php
    namespace system\connectors;

    use system\Interfaces\DBC;
    use System\system;

    /**
     * Clase de conección a BD MySQL, contiene los metodos y acceso a la información de connecióna la misma.
     * @param Object $connection Instancia de conección MySQL.
     */
    class MySQL implements DBC{
        private $connection;
        /**
         * Función de conección (Factory de la clase)
         * @access public
         * @return MySQL Objecto de conexión a BD.
         */
        public static function Connect(){
            return new self();
        }
        /**
         * Constructor de la clase
         * @access private
         * @param String|NULL $config ID de configuración, por defecto NULL (default).
         * @return Void Retorno vacío.
         */
        private function __construct($config=NULL){
            if($config==NULL || !is_string($config))
                $config='default';//configuración por defecto
            $config=json_decode(file_get_contents(System::dirBase().'/system/config/database.json'),true)[$config];
            $this->connection=new \mysqli($config["server"], $config["user"], $config["password"], $config["database"]);
        }
        /**
         * Devuelve una respuesta a la query dada a la función
         * @access public
         * @param String $query consulta SQL en texto plano
         * @return Array[]|String|NULL Retorna un array formateado como respuesta, NULL si la query no es funcional, 'empty' si la query da una respuesta sin registros.
         */
        public function query($query){
            $query=str_replace(['#',';'],'&quote',$query);//quitar comentarios evita la inyeccion SQL
            $queryType=strtolower(explode(' ',$query)[0]);
            $response=$this->connection->query($query);
            switch($queryType){
                case 'insert'||'update'||'delete':
                    $return=$this->requestQuery($response);
                    break;
                case 'select':
                    $return=$this->responseQuery($response);
                    break;
                default:
                    $return=NULL;
                    break;
            }
            return $return;
        }
        /**
         * Da formato a consulta de tipo petición a la BD, retorna un Array formateado encaso de exito, de lo contrario devolverá un String con un mensaje(empty [vacío] o unknown [no procesado])
         * @access private
         * @param Array[] $response Arreglo de respuesta de consulta en crudo.
         * @return Array[]|NULL Retorno de respuestas formateadas
         */
        private function responseQuery(Array $response){
            if($response!=false){
                $buffer=null;
                $return=null;
                if($response->num_rows==0)
                    $return='empty';//devolverá empty si no encontró nada en la BD que devolver
                else{
                    $return=array();
                    while($row = $response->fetch_row()){
                        $buffer=array();
                        foreach($row as $item){
                            array_push($buffer,$item);
                        }
                        array_push($return,$buffer);
                    }
                }
            }else
                $return=NULL;
        }
        /**
         * Da formato a una consulta de tipo petición a la BD, retornando un booleano (True exit, False Fracaso) o nulo (no procesado o no corresponde).
         * @access private
         * @param Array[] Arreglo de respuesta de consulta en crudo.
         * @return Bool|String|NULL Retorno de respuesta de ejecución de query, nulo en caso de 
         */
        private function requestQuery(Array $response){
            switch($response){
                case $response!='unknow' && $response!=FALSE:
                    $return=TRUE;
                    break;
                case NULL:
                    $return=NULL;
                default:
                    $return=FALSE;
            }
            return $return;
        }
        /**
         * Función que retorna un booleano como estado de la conexión.
         * @access public
         * @return Bool TRUE si exite error en la conexión, FALSE si no hay error en la conexión.
         */
        public function errorInConnection(){
            if($this->connection->connect_errno){
                return TRUE;
            }else{
                return FALSE;
            }
        }
        /**
         * Extrae el código de error de conexión en caso de haberlo.
         * @access public
         * @return Integer Código de error, 0 en caso de no presentar error.
         */
        public function getError(){
            if($this->connection->connect_errno)
                return $connection->connect_errno;
            else
                return 0;
        }
        /**
         * Finaliza la conexión con la BD.
         * @access public
         * @return Void Retorno vacío.
         */
        public function end(){
            $this->connection->close();
        }
        /**
         * Ejecuta un ping con la BD.
         * @access public
         * @return Integer Retorno de respuesta del ping.
         */
        public function ping(){
            return $this->connection->ping();
        }
    }