<?php
    namespace system;

    use system\ErrorHandler;

    //Clase que contiene las funcionalidades básicas del sistema en general que no contemnpla PHP o que requieren un tratamiento diferente
    //para simplificar su implementación.
    class System{
        //Devuelve la ruta base absoluta para el proyecto
        public static function dirBase(){
            $path=__DIR__;
            $path=str_replace('\\','/',$path);
            $path=str_replace('/system','',$path);
            return $path;
        }

        //Devuelve lista de directorios dentro de un directorio determinado
        public static function listDir($path){
            $list=[];
            if(file_exists($path)){
                if($files=opendir($path)){
                    while(false!==($file=readdir($files))){
                        if($file!='.' && $file!='..')
                            array_push($list,$file);
                    }
                    closedir($files);
                    return $list;
                }else
                    return null;
            }else
                return false;
        }

        //Función que lee un archivo y devuelve su contenido como un String
        public static function read($src){
            $content=null;
            if(strlen(str_replace(' ','',$src))>3){
                $file=fopen($src,'r');
                $content="";
                while(!feof($file)){
                    $content=$content.fgets($file);
                }
                fclose($file);
            }else print "Ruta no valida\n";
            return $content;
        }

        public static function autoload($function){
            spl_autoload_register($function($className));
        }
        //Función que permite escribir un archivo dado la ruta y el contenido que se va a escribir
        public static function write($src,$content){
            $file=fopen($src,'w+');
            fwrite($file,$content);
            fclose($file);
        }

        //Manejadores de excepciones
        public static function Error(){
            print "Hay un error";
            return true;
        }

        public static function Warning($errno,$errstr,$errfile,$errline,$errContext=null){
            //print "Hay una alerta en ".$errfile." en la linea ".$errline."<br />";
            $msg=NULL;
            $msg=ErrorHandler::Warning($errfile,$errline);
            header('HTTP/1.1 500');
            if($msg!=null)
                exit($msg);
            else
                exit("Ha surgido un error desconocido <br />\n".
                      "Este se trata de un error generico no reportado, por favor ingrese la excepción para los siguiente datos: <br />".
                      "Tipo: Warning <br />\n".
                      "Mensaje: ".$errstr." <br />\n".
                      "Archivo: ".$errfile." <br />\n".
                      "Linea: ".$errline." <br />\n");
            return true;
        }

        public static function notice(){
            print "Hay un aviso";
            return true;
        }

        public static function input($str){
            print $str;
            $handler=fopen("php://stdin","r");
            $input=fgets($handler,256);
            $input=rtrim($input);
            fclose($handler);
            return $input;
        }
        
    }