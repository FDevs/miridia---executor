<?php
    namespace system;

    use system\Routing;

    //Clase base para el archivo de rutas
    class Router{
        public $routes;//Almacena las rutas mediante un objeto <Routing>
        //Constructor de la clase
        public function __construct(){
            /*No tocar*/
            $this->routes=new Routing();
        }
        //Función que permite el acceso a una "página" por medio de su URL.
        //Internamente procesa y executa una función controladora devolviendo una vista.
        public function routingExecuting($URI){
            /*No tocar*/
            $res=$this->routes->returnRoute($URI);
        }
        //devuelve todas las rutas
        public function getRoutes($type){
            return $this->routes->getRoutes($type);
        }
    }