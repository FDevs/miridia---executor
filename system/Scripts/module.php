<?php
    namespace system\Scripts;

    use system\Console;
    use system\System;

    class module{
        public static function remove($module){
            if(file_exists(System::dirBase()."/system/modules/".$module.".php")){
                try{
                    $remove=call_user_func(array("system\\modules\\".$module,"Remove"));
                    if($remove)
                        Console::println("Modulo eliminado exitosamente","green");
                    else
                        Console::println("Error: no se pudo eliminar el módulo\n -Asegurese de que el modulo exista\n -Asegurese de que el nombre está bien escrito","red");
                }catch(Exception $e){
                    Console::println($e,"green");
                }
            }else
                Console::println("Modulo no encontrado","red");
        }
        public static function install($module){
            try{
                $resConfig=call_user_func(array("system\\modules\\".$module,"Config"));
                $resInstall=call_user_func(array("system\\modules\\".$module,"Install"));
                if(($resConfig || $resConfig==NULL) && ($resInstall || $resInstall==NULL))
                    Console::println("Modulo instalado correctamente","green");
                else if(!$resConfig)
                    Console::println("Error al generar la configuración inicial","red");
                else if(!resInstall)
                    Console::println("Error al instalar sub modulos de ".$module,"red");
                else
                    Console::println("Error desconocido al intentar instalar modulo");
            }catch(Exception $ex){
                Console::println($e,"green");
            }
        }
    }