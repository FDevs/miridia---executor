<?php
    namespace system\Scripts;

    use system\Console;
    use system\System;

    use src\Routes;

    class route{
        public static function new($uri,$controller){
            if(file_exists("src/Routes.php")){
                $route=new Routes();
                $route->routing();
                $res=$route->getRoutes();
                foreach($res as $route){
                    if($route==$uri){
                        Console::print("Esta ruta ya ha sido ingresada, intente otra","red");
                        exit();
                    }
                }
                $nRoute='            $this->routes->GET'."('".$uri."','".$controller."');\n".
                "        }\n".
                "    }";
                $file=System::read("src/Routes.php");
                $file=str_replace("    }","",$file);
                $content=$file.$nRoute;
                System::write("src/Routes.php",$content);
                Console::print("Ruta ingresada exitosamente","green");
            }else
                Console::print("Error: no existe el archivo de rutas","red");
        }

        public static function create(){
            Console::println("Verificando la existencia del archivo de rutas...",'blue');
            if(!file_exists("src/Routes.php")){
                $content="<?php"."\n"
                         ."    namespace src;" ."\n\n"
                         ."    use system\Router;"."\n\n"
                         ."    //Clase principal que contiene las rutas del proyecto"."\n"
                         ."    class Routes extends Router{"."\n"
                         ."        public function routing(){"."\n"
                         ."            /*Rutas personalizadas aquí*/"."\n\n"
                         ."        }"."\n"
                         ."    }"."\n";
                System::write('src/Routes.php',$content);
                Console::println("Archivo de rutas creado correctamente","green");
            }else
                Console::println("Aviso: archivo de rutas ya creado",'yellow');
        }

        public function get(){
            $route=new Routes();
            $route->routing();
            $res=$route->getRoutes();
            Console::println("-----------------------------------------------------------","yellow");
            Console::println("                Lista de rutas implementadas","blue");
            Console::println("-----------------------------------------------------------","yellow");
            foreach($res as $route){
                Console::println($route,"green");
            }
            Console::println("-----------------------------------------------------------","yellow");
        }
    }