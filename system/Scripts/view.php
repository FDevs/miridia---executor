<?php
    namespace system\Scripts;

    use system\Console;
    use system\System;

    class view{
        public static function new($type,$name,$lang='es',$charset='utf-8'){
            $dirBase=str_replace('\\', '/', __DIR__);
            $dirBase=str_replace('/system/Scripts','',$dirBase);
            if($name!=null){
                switch(ucfirst($type)){
                    case 'Page':
                        if(!is_dir($dirBase.'/src/view/Page'))
                            mkdir($dirBase.'/src/view/Page');
                        $content="<!DOCTYPE html>\n".
                        '<html lang="'.$lang.'">'."\n".
                        "    <head>\n".
                        '        <meta charset="'.$charset.'">'."\n".
                        '        <meta http-equiv="X-UA-Compatible" content="IE=edge">'."\n".
                        '        <meta name="viewport" content="width=device-width, initial-scale=1.0">'."\n".
                        "       <title>@title</title>\n".
                        "    </head>\n".
                        "   <body>\n".
                        "        @content\n".
                        "    </body>\n".
                        "</html>";
                        System::write('src/view/Page/'.$name.".html",$content);
                        Console::println("Página creada con exito","green");
                        break;
                    case 'Content':
                        if(!is_dir($dirBase.'/src/view/Content'))
                            mkdir($dirBase.'/src/view/Content');
                        $content='@title{send}';
                        System::write('src/view/Content/'.$name.".html",$content);
                        Console::println("Contenido creado con exito","green");
                        break;
                    case 'Component':
                        if(!is_dir($dirBase.'/src/view/Component'))
                            mkdir($dirBase.'/src/view/Component');
                        $content='';
                        System::write('src/view/Content/'.$name.".html",$content);
                        Console::println("Componente creado con exito","green");
                        break;
                    default:
                        Console::println("Tipo de elemento no existe","red");
                        break;
                }
            }else
                Console::print("No ha especificado un nombre para el elemento","red");
        } 
        public static function remove($type,$name){
            $type=ucfirst($type);
            if($name!=null){
                if(is_file("src/view/".$type."/".$name.".html")){
                    unlink("src/view/".$type."/".$name.".html");
                    if(!is_file("src/view/".$type."/".$name.".html"))
                        Console::println("Elemento eliminado con exito","green");
                    else
                        Console::println("Ha ocurrido un imprevisto que ha impedido la eliminación del elemento","red");
                }else
                Console::println("Elemento especificado no existe","red");
            }else 
            Console::println("No ha especificado un nombre para el elemento a eliminar","red");
        }
    }