<?php
    namespace system\Scripts;

    use system\Console;
    use system\System;

    class server{
        public static function start(){
            Console::println("Servidor activo...","blue");
            $data=json_decode(file_get_contents(System::dirBase().'/system/config/general.json'),true);
            $con='"'.$data['server'].':'.$data['onPort'].'"';
            Console::println("Activo en: http://".$data['server'].":".$data['onPort'],"green");
            Console::println('php -S '.$con.' '.System::dirBase().'/system/SystemDirectory.php');
            system('php -S '.$con.' '.System::dirBase().'/system/SystemDirectory.php');
        }
    }