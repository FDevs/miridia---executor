<?php
    namespace system\Scripts;

    use system\System;

    class script{
        public static function new($name){
            $contentFile="<?php\n".
            "    namespace system\Scripts;\n\n".
            "    use system\Console;\n\n".
            "    class ".$name."{\n".
            "    }";
            system::write("system/Scripts/".$name.".php",$contentFile);
        }
        public static function add($script,$argv='null'){
            [$name,$function]=explode('=>',$script);
            if($argv!="null"){
                $argv="$".$argv;
                $argv=str_replace('|',',$',$argv);
            }else $argv="";
            if(file_exists("system/Scripts/".$name.".php")){
                if(!method_exists("system\\Scripts\\".$name,$function)){
                    $content="    public static function ".$function."(".$argv."){\n".
                    "\n        }\n    }";
                    $file=system::read("system/Scripts/".$name.".php");
                    $file=substr($file,0,-1,);
                    $content=$file.$content;
                    system::write("system/Scripts/".$name.".php",$content);
                    print "creado exitosamente";
                }else print "Ya existe un metodo con el mismo nombre";
            }else print "Script nop existe";
        }
        public static function remove($name){
            if(is_file("system/Scripts/".$name.".php")){
                unlink("system/Scripts/".$name.".php");
                if(!is_file("system/Scripts/".$name.".php"))
                    print "Script eliminado con exito";
                else
                    print "Ha ocurrido un error en la eliminacion del script";
            }else
                print "- No existe el script";
        }
    }