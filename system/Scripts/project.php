<?php
    namespace system\Scripts;
    /*
        Script que facilita la inicialización de un proyecto creando los directorios faltantes a partir de la configuración
    */
    use system\Console;
    use system\System;

    class project{
        public static function routingGenerate(){
            $dirBase=str_replace('\\', '/', __DIR__);
            $dirBase=str_replace('/system/Scripts','',$dirBase);
            //-------------------------------------------------------------------------------------
            if(!is_file('src/Routes.php')){
                $content="<?php\n".
                            "namespace src;\n\n". 
                            "use system\Router;\n\n".
                            "//Clase principal que contiene las rutas del proyecto\n".
                            "class Routes extends Router{\n".
                            "    public function routing(){\n".
                            "        /*Rutas personalizadas aquí*/\n".
                                "}\n".
                        "}"; 
                System::write($dirBase.'/src/Routes.php',$content);
                Console::println("Registro de rutas inicializada","green");
            }else console::println("Registro de rutas ya existe","grey");
        }
        public static function init(){
            $dirBase=System::dirBase();
            $config=json_decode(file_get_contents($dirBase.'/system/config/modules.json'),true);
            //-------------------------------------------------------------------------------------
            console::println("===Inicializando módulos===","blue");
            self::initModules($config,$dirBase);
            console::println("===Inicializando recursos===","blue");
            self::initSRC($dirBase);
            self::routingGenerate();
        }
        private static function initSRC($dirBase){
            $dirList=["src","src/controller","src/view"];
            foreach ($dirList as $dir) {
                if(!file_exists($dirBase."/".$dir)){
                    mkdir($dirBase."/".$dir);
                    console::println($dir." creado con exito","green");
                }else
                    console::println($dir." ya existe","grey");
            }
        }
        private static function initModules($config,$dirBase){
            if(!file_exists("public"))
                mkdir("public");
            if(count($config)>0){
                foreach($config as &$conf){
                    if(!file_exists($dirBase.$conf["dir"])){
                        if(mkdir($dirBase.$conf["dir"]))
                            console::println($conf["dir"]." creado","green");
                        else
                            console::println("Error al crear ".$conf["dir"].", favor confirmar permisos del directorio o acceso al mismo","red");
                    }else
                        console::println($conf["dir"]." ya está creado","grey");
                }
            }else
                console::println("Sin módulos que inicializar","grey");
        }
    }