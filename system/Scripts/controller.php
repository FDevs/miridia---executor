<?php
    namespace system\Scripts;

    use system\System;
    use system\Console;

    class controller{
        public static function new($name=null){
            if($name!=null){
                $name=ucfirst($name);
                $content="<?php\n".
                "    namespace src\controller;\n\n".
                "    class ".$name."{\n".
                "    }";
                System::write('src/controller/'.$name.".php",$content);
                Console::println("Controlador creado con exito","green");
            }else
                Console::print("No ha especificado un nombre para el controlador","red");
        }
        public static function add($controller,$argv=false){
            [$controller,$function]=explode('=>',$controller);
            if($argv)
                $argv='$get';
            else
                $argv='';
            if(file_exists("src/controller/".$controller.".php")){
                if(!method_exists("src\\controller\\".$controller,$function)){
                    $content="    public static function ".$function."(".$argv."){\n";
                    if($addPage){
                        $content=$content.'        $page=HTML::Page('."'default','welcome'".');'."\n".
                        '        return $page->html;'."\n";
                    }
                    $content=$content."\n        }\n    }";
                    $file=System::read("src/controller/".$controller.".php");
                    $file=substr($file,0,-1,);
                    $content=$file.$content;
                    System::write("src/controller/".$controller.".php",$content);
                    Console::println("Funcion controladora creada exitosamente","green");
                }else 
                    Console::println("Función controladora ya existe","red");
            }else 
                Console::println("Controlador no existe","red");
        }
        public static function remove($name=null){
            if($name!=null){
                $name=ucfirst($name);
                if(is_file("src/controller/".$name.".php")){
                    unlink("src/controller/".$name.".php");
                    if(!is_file("src/controller/".$name.".php"))
                        Console::println("Controlador eliminado con exito","green");
                    else
                        Console::println("Ha ocurrido un imprevisto que ha impedido la eliminación del controlador","red");
                }else
                Console::println("Controlador especificado no existe","red");
            }else 
            Console::println("No ha especificado un nombre para el controlador a eliminar","red");
        }
    }