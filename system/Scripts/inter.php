<?php
    namespace system\Scripts;

    use system\System;
    use system\Console;

    class inter{
        public static function start(){
            $input="";
            $continue=True;
            while($continue){
                Console::print("\n");
                $input=Console::input(">","blue");
                switch($input){
                    case "exit":
                        $continue=False;
                        break;
                    default:
                        try{
                            eval($input);
                        }catch(ParseError $e){
                            Console::print("Codigo PHP no reconocido","red");
                        }
                        break;
                }
            }
        }
    }