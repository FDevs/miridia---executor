<?php
    namespace system\modules;

    //Permite el manejo de archivos JavaScript (JS)
    class JS{
        //Devuelve archivo JS si éste es válido
        public static function Get($src){
            try{
                $dir=str_replace("\\","/",__DIR__);
                $dir=str_replace("/system/modules","",$dir);
                $e=explode('.',$src);
                $extension=$e[count($e)-1];
                if(count($e)>=2){
                    if(self::valideExtension($extension)){
                        $config=json_decode(file_get_contents($dir.'/system/config/modules.json'),true)["js"];
                        $name=$dir.$config["dir"].$src;
                        if(is_file($name)){
                            $JSFile=fopen($name,'rb');
                            $lastModified=filemtime($name);
                            $etagFile = md5_file($name);//eTag del archivo
                            $_SERVER['HTTP_IF_MODIFIED_SINCE']=(isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) ? $_SERVER['HTTP_IF_MODIFIED_SINCE'] : $lastModified);
                            $ifModifiedSince = (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) ? $_SERVER['HTTP_IF_MODIFIED_SINCE'] : false);
                            $etagHeader = (isset($_SERVER['HTTP_IF_NONE_MATCH']) ? trim($_SERVER['HTTP_IF_NONE_MATCH']) : false);//eTag del Header
                            //Inicio de asignación a Header
                            header('Content-Type:application/javascript');//asignación de tipo de contenido
                            header("Content-Length:".filesize($name));//asignación de tamaño de archivo
                            header("Last-Modified: ".gmdate("D, d M Y H:i:s", $lastModified)." GMT");//asignación de fecha de ultima modificación del archivo
                            header("Etag: ".$etagFile);//asignación de eTag (Forma de identificar caché)
                            //header('Cache-control: public');//asignación de control de caché como público (puede almacenarse en cualquier caché)
                            //Fin de asignación a Header
                            if (@strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE'])==$lastModified || $etagHeader == $etagFile)
                                header('HTTP/1.1 304 Not Modified');//en caso de no haber modificaciones o el 'tagFile' no ha cambiado devuelve estado 304 (Ya estaba en caché del cliente)
                            else
                                header("HTTP/1.1 200"); //estado 200 en caso de ser pedido un archivo que no esté en caché del cliente
                            return fpassthru($JSFile);//termina de escribir información a puntero
                        }else
                            return null;
                    }else
                        return null;
                }else return null;
            }catch(Exception $ex){
                return null;
            }
        }
        //Devuelve true si la extención es válida, de lo contrario devuelve falso
        private static function valideExtension($ext){
            if($ext=="js")
                return true;
            return false;
        }
    }