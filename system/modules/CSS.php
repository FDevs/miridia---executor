<?php
    namespace system\modules;

    use system\Interfaces\Module;

    /**
     * Modulo que permite el manejo de archivos CSS
     */
    class CSS implements Module{
        //Devuelve archivo CSS si éste es válido
        /**
         * Función de acceso desde URI o externo, devuelve el archivo CSS si es válido como tal.
         * @access public
         * @static
         * @return Mixed devuelve el recurso solicitado según modulo.
         */
        public static function Get($src){
            try{
                $dir=str_replace("\\","/",__DIR__);
                $dir=str_replace("/system/modules","",$dir);
                $e=explode('.',$src);
                if(count($e)==2){
                    $nameFile=$e[0];
                    $extension=$e[1];
                    $config=json_decode(file_get_contents($dir.'/system/config/modules.json'),true)["css"];
                    if(CSS::valideExtension($extension,$config)){
                        $name=$dir.$config["dir"].$nameFile.'.'.$extension;
                        if(is_file($name)){
                            $CSSFile=fopen($name,'rb');
                            $lastModified=filemtime($name);
                            $etagFile = md5_file($name);//eTag del archivo
                            $_SERVER['HTTP_IF_MODIFIED_SINCE']=(isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) ? $_SERVER['HTTP_IF_MODIFIED_SINCE'] : NULL);
                            $ifModifiedSince = (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) ? $_SERVER['HTTP_IF_MODIFIED_SINCE'] : false);
                            $etagHeader = (isset($_SERVER['HTTP_IF_NONE_MATCH']) ? trim($_SERVER['HTTP_IF_NONE_MATCH']) : false);//eTag del Header
                            //Inicio de asignación a Header
                            header('Content-Type:text/'.$extension);//asignación de tipo de contenido
                            header("Content-Length:".filesize($name));//asignación de tamaño de archivo
                            header("Last-Modified: ".gmdate("D, d M Y H:i:s", $lastModified)." GMT");//asignación de fecha de ultima modificación del archivo
                            header("Etag: ".$etagFile);//asignación de eTag (Forma de identificar caché)
                            header('Cache-control: public');//asignación de control de caché como público (puede almacenarse en cualquier caché)
                            //Fin de asignación a Header
                            if (@strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE'])==$lastModified || $etagHeader == $etagFile)
                                header('HTTP/1.1 304 Not Modified');//en caso de no haber modificaciones o el 'tagFile' no ha cambiado devuelve estado 304 (Ya estaba en caché del cliente)
                            else
                                header("HTTP/1.1 200"); //estado 200 en caso de ser pedido un archivo que no esté en caché del cliente
                            return fpassthru($JSFile);//termina de escribir información a puntero
                        }else
                            return null;
                    }else
                        return null;
                }else return null;
            }catch(Exception $ex){
                return null;
            }
        }
        /**
         * Crea/sobreescribe archivo de configuración (JSON) desde system/config.
         * @access public
         * @static
         * @return Bool TRUE si la configuración fue cargada con exito, FALSE si no fue cargada con exito
         */
        public static function Config(){
            //empty
        }
        /**
         * Procedimiento de instalación del módulo, esto puede incluir:
         *      1. Creación de directorios y archivos mínimos necesarios.
         *      2. Creación/integración de configuración necesaria (valores por defecto).
         * @access public
         * @static
         * @return Bool TRUE si la instalación fue exitosa, FALSE en caso contrario.
         */
        public static function Install(){
            //empty
        }
        /**
         * Procedimiento de eliminación del modulo, también debe procurar eliminar lo sdirectorios y archivos huerfanos.
         * @access public
         * @static
         * @return Bool TRUE si el modulo fue eliminado con exito, False en caso contrario
         */
        public static function Remove(){
            //empty
        }
        /**
         * Valida la extención de un archivo
         * @access private
         * @static
         * @return Bool True si la extención es valida, FALSE en caso contrario
         */
        private static function valideExtension($ext,$cssConfig){
            error_reporting(E_ERROR | E_PARSE);
            try{
                if($cssConfig["extencion"]==$ext)
                    return true;
                return false;
            }catch(Exception $e){
                return false;
            }
        }
    }