<?php
    namespace system\modules;

    use system\Interfaces\Module;
    use system\Interfaces\DBC;
    
    use system\System;

    class Scribe implements Module{
        private $config=NULL;
        private DBC $DBC;
        private static $connector;
        //=========================================================================================================
        public static function GET(){
            return NULL;
        }
        public static function Config(){
            $res=false;
            $config="{\n".
                    '    "name":""'."\n".
                    '    "database":""'."\n".
                    '    "server":""'."\n".
                    '    "port":""'."\n".
                    '    "user":""'."\n".
                    '    "password":""'."\n".
                    '}';
            try{
                if(!file_exists(System::dirBase().'/system/config/database.json'))
                    System::write(System::dirBase().'/system/config/database.json',$config);//mysql,postgres,...
                return true;
            }catch(Exception $e){
                return false;
            }
        }
        public static function Install(){
            //crear configuracion inicial
            return NULL;
        }
        public static function Remove(){
            try{
                return unlink(System::dirBase().'/system/modules/Scribe.php');
            }catch(Exception $e){
                return false;
            }
        }
        //=========================================================================================================
        public static function Connect(){
            return new self();
        }
        private function __construct(){
            $this->config=json_decode(file_get_contents(System::dirBase().'/system/config/database.json'),true);
            $this->DBC=call_user_func(array('system\\connectors\\'.$this->config['name'],'Connect'));
        }
        public function getConnector(){
            return $this->DBC;
        }
    }