<?php
    namespace system\modules;

    use system\System;

    //Modulo que almacena funcionalidades para el manejo de imagenes
    class Image{
        public static function Get($file,$config="image"){
            try{
                $dir=str_replace("\\","/",__DIR__);
                $dir=str_replace("/system/modules","",$dir);
                if(count(explode('.',$file))>=2)
                    [$nameFile,$extension]=explode('.',$file);
                else
                    return null;
                if(Image::validExtension($extension,$config)){
                    $path=json_decode(file_get_contents($dir.'/system/config/modules.json'),true)[$config]["dir"];
                    $path=$dir.$path;
                    $name=$path.$nameFile.'.'.$extension;
                    if(is_file($name)){
                        $fp=fopen($name,'rb');
                        $lastModified=filemtime($name);
                        $etagFile = md5_file($name);//eTag del archivo
                        $_SERVER['HTTP_IF_MODIFIED_SINCE']=isset($_SERVER['HTTP_IF_MODIFIED_SINCE'])? $_SERVER['HTTP_IF_MODIFIED_SINCE']:$lastModified;
                        $etagHeader = (isset($_SERVER['HTTP_IF_NONE_MATCH']) ? trim($_SERVER['HTTP_IF_NONE_MATCH']) : false);//eTag del Header
                        //Inicio de asignación a Header
                        header("Last-Modified: ".gmdate("D, d M Y H:i:s", $lastModified)." GMT");//asignación de fecha de ultima modificación del archivo
                        header("Etag: ".$etagFile);//asignación de eTag (Forma de identificar caché)
                        header('Cache-control: public');//asignación de control de caché como público (puede almacenarse en cualquier caché)
                        //Fin de asignación a Header
                        if($extension!='ico')
                            header('Content-Type:image/'.$extension);
                        header("Content-Length:".filesize($name));
                        if (@strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE'])==$lastModified || $etagHeader == $etagFile)
                            header('HTTP/1.1 304 Not Modified');//en caso de no haber modificaciones o el 'tagFile' no ha cambiado devuelve estado 304 (Ya estaba en caché del cliente)
                        else
                            header("HTTP/1.1 200"); //estado 200 en caso de ser pedido un archivo que no esté en caché del cliente
                        return fpassthru($fp);//termina de escribir información a puntero
                    }else
                        return null;
                }else
                    return null;
            }catch(Exception $ex){
                return null;
            }
        }
        public static function favicon(){
            header('Content-Type:image/x-icon');
            return self::Get('favicon.ico','favicon');
        }
        public static function validExtension($ext,$config){
            error_reporting(E_ERROR | E_PARSE);
            try{
                $dir=str_replace("\\","/",__DIR__);
                $dir=str_replace("/system/modules","",$dir);
                $config=json_decode(file_get_contents($dir.'/system/config/modules.json'),true)[$config];
                $extensions=$config["extension"];//extensiones de imagenes admitidas
                //
                if($extensions[$ext]=="accept"){
                    return true;
                }
                return false;
            }catch(Exception $e){
                return false;
            }
        }
    }