<?php
    namespace system\modules;

    use system\System;
    use system\Interfaces\Module;
    
    //Modulo que almacena funcionalidades para manejo de archivos HTML
    class HTML implements Module{
        //Devuelve el contenido HTML de un archivo .html
        private $html;
        private $flags=[];
        private $specialChar=[
            '\n'=>'<br>',
            '\line'=>'<hr>'
        ];
        public function __construct($src){
            $this->html=self::getContent('Page/'.$src);
        }
        public static function GET(){
            //No requiere acceso externo
        }
        public static function Config(){
            return null;
        }
        public static function Install(){
            return null;
        }
        public static function Remove(){
            return unlink(System::dirBase().'/system/modules/HTML.php');
        }
        //-------------------------------------------------------------------------------------------
        private static function getContent($src){ 
            $path=System::dirBase();
            $file=system::read($path.'/src/view/'.$src.'.html');
            return $file;
        }

        public static function Page($html,$content=null){
            $page=new self($html);
            if($content!=null){
                [$content,$title]=$page->getTitlePage($content);
                $page->add(["title"=>$title,"content"=>$content]);
            }
            //$page->render();
            return $page;
        }

        //devuelve un HTML con un flag reemplazado por un contenido
        public function add($flag,$content=null){
            if(!is_array($flag)){
                $this->html=str_replace('@'.$flag,$content,$this->html);
                if(key_exists($flag,$this->flags))
                    unset($this->flag[$flag]);
            }else if(is_array($flag)){
                foreach($flag as $element){
                    $this->html=str_replace('@'.key($flag),$element,$this->html);
                    if(key_exists(key($flag),$this->flags))
                        unset($this->flag[key($flag)]);
                    next($flag);
                }
            }
        }

        private function getTitlePage($content){
            $title="";
            $page="";
            $content=$this->getContent("Content/".$content);
            $bufferPage=explode("\n",$content);
            foreach($bufferPage as $element){
                if(substr($element,0,6)=="@title"){
                    $title=str_replace('@title{','',$element);
                    $title=str_replace('}','',$title);
                }else
                    $page=$page.$element;
            }
            return [$page,$title];
        }
        //Permite extraer el contenido interno añadido del código html junto a los flags del usuario
        public function mapInnerFlags(){
            $innerFlag=[];
            $state=0;/*sirve de control para ingresar valor al nombre o al contenido interno del flag;
                0: es solo html
                1: es un nombre de flag
                2: es un contenido de flag
            */
            $bufferFlagName="";
            $bufferFlagContent="";
            $page=$this->html;
            $bufferPage="";
            for($i=0;$i<strlen($page);$i++){
                switch($page[$i]){
                    case "@":
                        $state=1;
                        break;
                    case "{":
                        $state=2;
                        break;
                    case "}":
                        $state=0;
                        break;
                    default:
                        switch($state){
                            case 0:
                                if($bufferFlagName!="" && $bufferFlagContent!=""){
                                    $innerFlag[$bufferFlagName]=$bufferFlagContent;
                                    $bufferFlagName="";
                                    $bufferFlagContent="";
                                }
                                $bufferPage=$bufferPage.$page[$i];
                                break;
                            case 1:
                                $bufferFlagName=$bufferFlagName.$page[$i];
                                break;
                            case 2:
                                $bufferFlagContent=$bufferFlagContent.$page[$i];
                                break;
                            default:
                                break;
                        }
                        break;
                }
            }
            if(count($this->flags)==0)
                $this->flags=$innerFlag;
            else
                $this->flags=array_merge($this->flags,$innerFlag);
            return $innerFlag;
        }
        public function removeInFlag(){
            foreach($this->flags as $flag){
                $value="@".key($this->flags)."{".$flag."}";
                $this->html=str_replace($value,"",$this->html);
                next($this->flags);
            }
            reset($this->flags);
        }
        //reemplaza caracteres especiales por etiquetas html que corresponda
        private function replaceSpecial(){
            foreach($this->specialChar as $special){
                $this->html=str_replace('.'.key($this->specialChar),'void',$this->html);
                $this->html=str_replace(key($this->specialChar),$special,$this->html);
                $this->html=str_replace('void',key($this->specialChar),$this->html);
                next($this->specialChar);
            }
            reset($this->specialChar);
        }
        //
        public function render(){
            $flags=$this->mapInnerFlags();
            $this->removeInFlag();
            $this->add($this->flags);
            $this->replaceSpecial();
            //var_dump($this->flags);
            return $this->html;
        }
    }
    