<?php
    namespace system\modules;

    use system\Interfaces\Module;
    use system\System;

    class Json implements Module{
        private $json=[];
        //--------------------------------------------------------------
        public static function GET(){
            return NULL;
        }
        public static function Config(){
            return NULL;
        }
        public static function Install(){
            return TRUE;
        }
        public static function Remove(){
            return unlink(System::dirBase().'/system/modules/HTML.php');
        }
        //---------------------------------------------------------------
        private function __construct($json){
            $this->json=$json;
        }
        public static function load($path){
            $raw=json_decode(file_get_contents(System::dirBase().'/'.$path),true);
            return new Json($raw);
        }
        public static function make($array){
            if(gettype($array)=="array")
                return new Json($array);
            else 
                return new Json(json_encode($array));
        }
        public function array(){
            return $this->json;
        }
        public function json($pretty=FALSE){
            return $pretty?json_encode($this->json,JSON_PRETTY_PRINT):json_encode($this->json);
        }
        public function response($pretty=""){
            header('Content-type:application/json');
            return $this->json($pretty=="format"?TRUE:FALSE);
        }
    }