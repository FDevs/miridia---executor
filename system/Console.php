<?php
    namespace system;

    class Console{
        const font=[
            "black"=>"[0;30",
            "grey"=>"[1;30",
            "red"=>"[0;31",
            "light red"=>"[1;31",
            "green"=>"[0;32",
            "ligth green"=>"[1;32",
            "brown"=>"[0;33",
            "yellow"=>"[1;33",
            "blue"=>"[0;34",
            "ligth blue"=>"[1;34",
            "magenta"=>"[0;35",
            "light magenta"=>"[1;35",
            "cyan"=>"[0;36",
            "ligth cyan"=>"[1;36",
            "light grey"=>"[0;37",
            "white"=>"[1;37"
        ];
        const background=[
            "black"=>";40",
            "red"=>";41",
            "green"=>";42",
            "yellow"=>";43",
            "blue"=>";44",
            "magenta"=>";45",
            "cyan"=>";46",
            "ligth grey"=>";47"
        ];
        private static function color($font="white",$back="black"){
            if(key_exists($font,self::font)){
                if(key_exists($back,self::background))
                    return self::font[$font].self::background[$back]."m";
                else
                    exit("No existe <".$back."> dentro de los colores permitidos para el fondo\n");
            }else
                exit('No existe <'.$font.'> dentro de los colores permitidos para la fuente'."\n");
        }
        public static function input($str,$font="white",$background="black"){
            self::print($str,$font,$background);
            $handler=fopen("php://stdin","r");
            $input=fgets($handler,256);
            $input=rtrim($input);
            fclose($handler);
            return $input;
        }
        public static function print($msg,$font="white",$background="black"){
            print "\e".self::color(strtolower($font),strtolower($background)).$msg."\e[0m";
        }
        public static function println($msg,$font="white",$background="black"){
            print "\e".self::color(strtolower($font),strtolower($background)).$msg."\e[0m"."\n";
        }
    }