<?php
    namespace system;

    use system\Route;
    use system\ModuleResolve;
    use system\System;
    use system\ErrorHandler;

    //Clase que contiene las funciones para almacenar, manejar rutas y ejecutar las funciones controladoras
    class Routing{
        public $routes=array();//Array de rutas, contiene los objetos de ruta.
        //Constructor de la clase.
        public function __construct(){
            //Aquí no hay nada
            $this->routes=(Object)['get'=>[],'post'=>[],'put'=>[],'delete'=>[],'update'=>[]];
        }

        //Retorna el controlador asociado a la URI con mayor similitud a las cargadas, si no lo encuentra retorna NULL
        public function getControllerAsociated(){
            [$isModule,$reference]=ModuleResolve::isModule($URI);
            if($isModule){
                return (Object)[
                    "type"=>"Module",
                    "class"=>$reference->module,
                    "function"=>$reference->controller,
                    "values"=>$reference->path
                ];
            }
            $res=NULL;
            $values=NULL;
            [$res,$values]=$this->getRoute($URI);
            if($res!=null){
                return (Object)[
                    "type"=>"controller",
                    "class"=>$res["route"]->class,
                    "function"=>$res["route"]->method,
                    "values"=>$values
                ];
            }     
            return NULL;
        }

        private function registryRoute($method,$URI,$var,$callback){
            if(strpos($URI,'<')==false && strpos($URI,'>')==false){
                array_push($this->routes->{strtolower($method)},["method"=>$method,"route"=>new Route($URI,$callback),"var"=>$var]);
            }else
                print("Error: los caracteres '<' o '>' No son aceptados para metodos ".$method."\n");
        }

        /**
         * Añade a la lista de rutas una nueva ruta, con metodo de recepción de datos mediante metodo [GET]
         * @access public
         * @param String $URI URI a procesar
         * @param String $callback Controlador asociado a URI
         * @return VOID Retorno vacío
         */
        public function GET($URI,$callback){
            $sub_URI=explode('/',$URI);//Separación de la URI para ser procesada
            $URI='';
            $buff=[];
            $i=0;//contador,simple, común y silvestre
            foreach($sub_URI as $sub){
                if(strlen($sub)>0){
                    if($sub[0]=='$'){
                        $buff['var'.$i]=substr($sub,1);
                        $URI=$URI.'/$var'.$i;
                        $i++;
                    }else
                        $URI=$URI.'/'.$sub;
                }
            }
            if(strlen($URI)==0)
                $URI='/';
            //Creación de nueva ruta, añadiendola a la lista de rutas
            array_push($this->routes->get,["method"=>"GET","route"=>new Route($URI,$callback),"var"=>$buff]);
        }

        /**
         * Añade a la lista de rutas una nueva ruta, con metodo de recepción de datos mediante metodo [POST]
         * @access public
         * @param String $URI URI a procesar
         * @param Array[String] $var Arreglo de variables admitidas por el controlador
         * @param String $callback Controlador asociado a URI
         * @return VOID Retorno vacío
         */
        public function POST($URI,$var,$callback){
            $this->registryRoute('POST',$URI,$var,$callback);
        }

        //Añade a la lista de rutas una nueva ruta, con metodo de recepción de datos mediante metodo [UPDATE]
        public function DELETE($URI,$callback){
            $this->registryRoute('DELETE',$URI,[],$callback);
        }

        //Añade a la lista de rutas una nueva ruta, con metodo de recepción de datos mediante metodo [UPDATE]
        public function PUT($URI,$callback){
            $this->registryRoute('PUT',$URI,[],$callback);
        }

        //Devuelve la lista de rutas completa
        public function getRoutes($type){
            $return=array();
            foreach($this->routes->{$type} as $route){
                array_push($return,$route["route"]->route);
            }
            return $return;
        }

        //
        /**
         * Compara 2 URIs dadas y retorna n° de coincidencias y las variables asociadas
         * @access public
         * @param String $route URI completa objetivo
         * @param String $comp URI dada a comparar
         * @return Array[] Retorna un arreglo de 2 items: $response que determina cuantas partes del URI coincide correctamente, $var que contiene las variables identificadas como tales contenidas en la misma URI de origen
         */
        public function compareURI($route,$comp){
            $uri_buffer=explode('/',$route['route']->route);
            $uri_buffer=array_filter($uri_buffer,function($var){
                if(strlen($var)>0)
                    return $var;
            });
            $comp_buffer=str_replace('?','/?',$comp);
            $comp_buffer=explode('/',$comp_buffer);
            $comp_buffer=array_filter($comp_buffer,function($var){
                if(strlen($var)>0)
                    return $var;
            });
            $var=[];//Arreglo que contiene las variables identificables asociadas a la ruta.
            $response=($uri_buffer==$comp_buffer && $comp=='/')?1:0; //Determina si es una ruta raiz, si es así asigna un -1.
            $i_var=0;
            foreach($uri_buffer as $buffer){
                if($buffer==current($comp_buffer)){
                    $response+=1;
                    next($comp_buffer);
                }else if(substr($buffer,1,3)=='var'){
                    $var[$route['var']["var".$i_var]]=current($comp_buffer);
                    $i_var++;
                    next($comp_buffer);
                }else{
                    reset($comp_buffer);
                }
            }
            return [$response,$var];
        }

        //Devuelve la ruta más probable por comparación
        private function getRoute($URI){
            $response=[null,null];
            $vars=array();
            switch($_SERVER['REQUEST_METHOD']){
                case 'GET':
                    $compare=0;
                    foreach($this->routes->get as $route){
                        [$compare_buffer,$vars]=$this->compareURI($route,$URI);
                        if(($compare<$compare_buffer && $compare_buffer>0)){
                            $response=[$route,$vars];
                            $compare=$compare_buffer;
                        }
                    }
                    break;
                case 'POST':
                    foreach($this->routes->post as $route){
                        if($route["route"]->route==$URI){
                            foreach($route["var"] as $var){
                                if(array_key_exists($var,$_POST))
                                    $vars[$var]=$_POST[$var];
                                else
                                    $vars[$var]=null;
                            }
                            $response=[$route,$vars];
                        }
                    }
                    break;
                case 'DELETE':
                    $compare=0;
                    foreach($this->routes->delete as $route){
                        [$compare_buffer,$vars]=$this->compareURI($route,$URI);
                        if($compare<$compare_buffer){
                            $response=[$route,$vars];
                            $compare=$compare_buffer;
                        }
                    }
                    break;
                case 'PUT':
                    foreach($this->routes->put as $route){
                        if($route["route"]->route==$URI){
                            $input=file_get_contents('php://input');
                            $vars=["input"=>$input];
                            $response=[$route,$vars];
                        }
                    }
                    break;
                default:
                    abort();
                    break;
            }
            return $response;
        }
        //Trata las URI para activar la función contrladora correspondiente, 
        //en caso de error activa algunos de los errores configurados (por defecto 404).
        public function returnRoute($URI){
            $values=null;
            [$module,$res]=ModuleResolve::Resolve($URI);
            if($module!=false){//Se evalua si es un modulo, fallo logico: corregir despues
                if($res!=null)
                    return $res;
                else
                    ErrorHandler::Error404();
            }else{
                [$res,$values]=$this->getRoute($URI);
                //---------------------------------
                if($res!=null){
                    $arr=[];
                    if($values!=null) array_push($arr,(Object)$values);
                    echo call_user_func_array(array('src\\controller\\'.$res["route"]->class,$res["route"]->method),$arr);
                }else
                    ErrorHandler::Error404();
            }
        }
    }