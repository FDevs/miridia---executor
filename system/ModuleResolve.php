<?php
    namespace system;

    //Clase que ayuda a resolver e identificar aquellos modulos que requieren acceso a recursos del directorio <<public>>
    //Ej: URI tipo /Image/*/*.jpg tiene acceso a los elementos del directorio /public/images/* y debe ser resuelto por ducho modulo
    class ModuleResolve{
        //Resuelve la URI por medio de su identificador
        public static function Resolve($URI){
            $isModule=false;
            $res=null;
            $dirBase=str_replace('\\', '/', __DIR__);
            $config=json_decode(file_get_contents($dirBase.'/config/modules.json'),true);

            //caso: es una referencia directa, ej /favicon.ico
            foreach($config as &$conf){
                if($URI==$conf["routeID"]){
                    $res=call_user_func(array('system\\modules\\'.$conf["controllerModule"],$conf["controllerFunction"]));
                    $isModule=true;
                }
            }
            //caso: es una referencia por identificador, ej /Image/a1.jpg
            $ID=ModuleResolve::Identify($URI);
            $path=str_replace("/".$ID,'',$URI);
            $path=str_replace('%20', ' ', $path);
            if(str_replace(' ','',$path)!="" && $isModule==false){
                foreach($config as &$conf){
                    if($ID==$conf["routeID"]){
                        $res=call_user_func(array('system\\modules\\'.$conf["controllerModule"],$conf["controllerFunction"]),$path);
                        $isModule=true;
                    }
                }
            }
            return [$isModule,$res];
        }
        //Identifica si la URI pertenece a un modulo, retorna 2 parametros: bool & array(Modulo + controlador)
        public static function isModule($URI){
            $isModule=false;
            $ref=NULL;
            $config=json_decode(file_get_contents(System::dirBase().'/config/modules.json'),true);
            foreach($config as $conf){
                if($URI==$conf["routeID"]){
                    $isModule=true;
                    $ref=(Object)["module"=>$conf["controllerModule"],"controller"=>$conf["controllerFunction"],"path"=>$path];
                }
            }
            $ID=self::Identify($URI);
            $path=str_replace("/".$ID,'',$URI);
            $path=str_replace('%20', ' ', $path);
            if(str_replace(' ','',$path) && !$isModule){
                foreach($config as &$conf){
                    if($ID==$conf["routeID"]){
                        $isModule=true;
                        $ref=(Object)["module"=>$conf["controllerModule"],"controller"=>$conf["controllerFunction"],"path"=>$path];
                    }
                }
            }   
            return [$isModule,$ref];
        }
        //devuelve el identificador de la URI
        public static function Identify($URI){
            $elements=explode("/",$URI);
            return $elements[1];
        }
    }