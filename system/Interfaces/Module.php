<?php
    namespace system\Interfaces;

    /**
     * Interfaz general de modulos.
     * IMPORTANTE: todos los modulos deben de tener esta interfaz integrada en su código.
     */
    interface Module{
        /**
         * Función de acceso desde URI o externo.
         * @access public
         * @return Mixed devuelve el recurso solicitado según modulo.
         */
        public static function GET();
        /**
         * Crea/sobreescribe archivo de configuración (JSON) desde system/config.
         * @access public
         * @return Bool TRUE si la configuración fue cargada con exito, FALSE si no fue cargada con exito
         */
        public static function Config();
        /**
         * Procedimiento de instalación del módulo, esto puede incluir:
         *      1. Creación de directorios y archivos mínimos necesarios.
         *      2. Creación/integración de configuración necesaria (valores por defecto).
         * @access public
         * @return Bool TRUE si la instalación fue exitosa, FALSE en caso contrario.
         */
        public static function Install();
        /**
         * Procedimiento de eliminación del modulo, también debe procurar eliminar lo sdirectorios y archivos huerfanos.
         * @access public
         * @return Bool TRUE si el modulo fue eliminado con exito, False en caso contrario
         */
        public static function Remove();
    }