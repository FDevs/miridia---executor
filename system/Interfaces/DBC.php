<?php
    namespace system\Interfaces;

    /**
     * Intefaz de conexión a BD (General)
     */
    interface DBC{
        /**
         * Función de conexión a BD (Factory class)
         * @access public
         * @return Object Instancia de conexión
         */
        public static function Connect();
        /**
         * Devuelve una respuesta a la query dada a la función
         * @access public
         * @param String $query consulta SQL en texto plano
         * @return Array[]|String|NULL Retorna un array formateado como respuesta, NULL si la query no es funcional, 'empty' si la query da una respuesta sin registros.
         */
        public function query($query);
        /**
         * Función que retorna un booleano como estado de la conexión.
         * @access public
         * @return Bool TRUE si exite error en la conexión, FALSE si no hay error en la conexión.
         */
        public function errorInConnection();
        /**
         * Extrae el código de error de conexión en caso de haberlo.
         * @access public
         * @return Integer Código de error, 0 en caso de no presentar error.
         */
        public function getError();
        /**
         * Finaliza la conexión con la BD.
         * @access public
         * @return Void Retorno vacío.
         */
        public function end();
        /**
         * Ejecuta un ping con la BD.
         * @access public
         * @return Integer Retorno de respuesta del ping.
         */
        public function ping();
    }