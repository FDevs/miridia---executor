<?php
    spl_autoload_register(function($className){
        $dir=__DIR__."\ ".$className.".php";
        $dir=str_replace('\\','/',$dir);
        $dir=str_replace('/system','',$dir);
        include str_replace(' ','',$dir);
    });
    //Clase de uso exclusivo de executor, 
    //donde están todas las funciones utilizadas por el asistente de linea de comandos anteriormente mencionado.
    class Exec{
        //Crea una nueva página con sus elementos respectivos (controlador y ruta), dado una vista previamente
        public static function newPage($name,$options=["controller"=>"-General"]){
            //
            $route=new src\Routes();
            $route->routing();
            $routes=$route->getRoutes();
            $exist=false;
            foreach($routes as $r){
                if(strcmp($r,"/".$name)==0)
                    $exist=true;
            }
            //
            if(!$exist){
                $options["controller"]=str_replace('-','',$options["controller"]);
                $res=Exec::newRoute($name,$options);
                if($res==0) 
                    $res=Exec::addController(ucwords($options["controller"]),$name);
                    switch($res){
                        case 0:
                            print "Controlador creado correctamente\n";
                            break;
                        case -1:
                            print "Error al crear controlador\n";
                            return -1;
                            break;
                        default:
                            print "Error desconocido al crear controlador\n";
                    }
                return 0;
            }else
                return 1;
        }
        //concatena una URL
        public static function nameCat($URI){
            $bufferName=explode('/',$URI);
            $bufferName=array_diff($bufferName,array(''));
            $i=0;
            $nameCat="";
            foreach($bufferName as $buffer){
                $buffer=str_replace('$','',$buffer);
                if($i==0){
                    $nameCat=$buffer;
                    $i++;
                }else
                    $nameCat=$nameCat.ucwords($buffer);
            }
            return $nameCat;
        }
        //Añade una nueva función controladora a su respectivo controlador, en caso de no existir un controlador esta funcion lo crea.
        public static function addController($controller,$name=null){// <Controlador>,<URI (ej: /welcome)>, <Nombre de la vista (obj controlador)>
            try{
                if($name!=null)
                    $nameCat=Exec::nameCat($name);//concatena la URI en una palabra para servir como nombre al controlador
                if(file_exists('src/controller/'.$controller.'.php')){//verifica si el controlador existe en src\controller
                    $new="<?php";//contenedor de nuevo codigo de controlador
                    $file=system\System::read("src/controller/".$controller.".php");//lectura de archivo controlador
                    $content=explode("\n",$file);//particionar el archivo por linea leida
                    //$isUse=false; //boleano para verificar si la vista se ha implementado antes
                    $i=0; //contador
                    for($i=1;$i<count($content)-1;$i++){
                        if(strcmp($content[$i],$content[2])!=0){
                            $buffer=str_replace('    ','',$content[$i]);
                            if(strcmp(substr($buffer,0,9),"namespace")==0)
                                $new=$new."\n".$content[$i]."\n";
                            else{
                                $new=$new."\n".$content[$i];
                            }
                        }
                    }
                    $function="\n        public static function ".$nameCat."Controller(){\n\n"
                        ."        }\n";
                    $new=$new.$function.'    }';
                }else{
                    $new="<?php\n"
                    ."    namespace src\\controller;\n\n"
                    ."    use system\modules\HTML;\n\n"
                    ."    class ".ucwords($controller)."{\n";
                    if($name!=null){
                        $new=$new."        public static function ".$nameCat."Controller(){\n\n"
                        ."        }\n";
                    }
                    $new=$new."    }";
                }
                system\System::write('src/controller/'.$controller.'.php',$new);
                return 0;
            }catch(Exception $e){
                print $e;
                return -1;
            }
        }
        //Añade una nueva ruta al archivo de rutas (Routes.php)
        public static function newRoute($name,$options=["controller"=>"General"]){
            try{
                $content=system\System::read("src/Routes.php");
                $subContent=explode(';',$content);
                $content="";
                $nameCat=Exec::nameCat($name);
                $nameCat=str_replace("$","",$nameCat);
                $i=0;
                foreach($subContent as $sub){
                    if($i<count($subContent)-1){
                        if($content=="")
                            $content=$content.$sub;
                        else
                            $content=$content.';'.$sub;
                        $i++;
                    }else
                        break;
                }
                if($name[0]=='/')
                    $name=substr($name,1,strlen($name)-1);
                $content=$content.";\n\n            ".'$this->routes->GET'."('/".$name."','".$options["controller"]."::".$nameCat."Controller');\n        }\n    }";

                system\System::write("src/Routes.php",$content);
                return 0;
            }catch(Exception $e){
                print $e;
                return -1;
            }
        }
    }