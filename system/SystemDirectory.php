<?php
    namespace system;
    
    //Función de autocarga de clases
    spl_autoload_register(function($className){
        $className=str_replace('\\','/',$className);
        $dirBase=str_replace('\\','/',__DIR__);
        $dirBase=str_replace('/system','',$dirBase);
        $dir=$dirBase."/".$className.".php";
        include $dir;
    });
    
    use src\Routes;

    set_error_handler("system\System::Warning",E_WARNING);
    //------------------------------------

    $url= $_SERVER["REQUEST_URI"];//Rescata la URI de la petición
    $route=new Routes();//Crea un nuevo objeto de rutas
    $route->routing();//Carga las rutas
    $route->routingExecuting($url);//devuelve la ruta (URI) solicitada activando su función controladora